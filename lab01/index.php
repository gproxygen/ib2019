<!DOCTYPE html>
<html dir="ltr" lang="ru">

<head>
    <title>ЛР</title>
    <meta name="viewport" charset=UTF-8 content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">    
    <script src="js/main.js"></script>
    <script src="js/jquery.js"></script>    
</head>

<body>

<div class="container">
<form id='regForm' action="actions.php" class="form-signin hide" method="POST">
    <input name="register" hidden>
    <h2 class='text-center'>Регистрация</h2>
    Имя    
    <input type="text" name="username" class="form-control" required>
    Логин (он же email)
    <input type="email" name="email" class="form-control" placeholder="E-mail" required>
    Пароль
    
    <input type="password" name="password" class="form-control" placeholder="Пароль" required>
    
    <p></p>
    <div class='col-8 offset-2'>
    <button class="btn btn-lg btn-success btn-block" type="submit">Выполнить</button> 
   
   
   
</div>
</form>

<form id ='authForm' action="actions.php" class="form-signin" method="POST">
    <input name="auth" hidden>
    <h2>Авторизация</h2>    
    Логин    
    <input type="text" name="login" class="form-control" required>    
    Пароль
    <input type="password" name="pass" class="form-control" required>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Выполнить</button> 
   
</form>
<label id ='chudo_knopka' onclick='setVisible()' class= 'offset-2'>Зарегистрироваться</label> 
</div>
</body>
</html>